// 基础路径 注意发布之前要先修改这里
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
module.exports = {
  lintOnSave: true,
  productionSourceMap: false,
  // configureWebpack: config => {
  //     if (process.env.NODE_ENV === 'production') {
  //         return {
  //             plugins: [
  //                 new BundleAnalyzerPlugin()
  //             ]
  //         }
  //     }
  // },
  devServer: {
    port: 8080, // 端口号
    hotOnly: true, // 热更新
    proxy: {
      '/api': {     //这里最好有一个 /
        target: 'http://localhost:8081',  // 后台接口域名
        ws: false,        //如果要代理 websockets，配置这个参数
        secure: false,  // 如果是https接口，需要配置这个参数
        changeOrigin: true,  //是否跨域
        pathRewrite: {
          '^/api':'/api'
        }
      }
    }
  },
  chainWebpack: (config) => {
    const entry = config.entry('app');
    entry
      .add('babel-polyfill')
      .end();
    entry
      .add('classlist-polyfill')
      .end();
    entry
      .add('@/mock')
      .end();
  },
  css: {
    extract: { ignoreOrder: true }
  }
};