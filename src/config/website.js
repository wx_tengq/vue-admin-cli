/**
 * 全局配置文件
 */
export default {
  title: 'Vue', // 标题
  logo: 'VUE', // 侧面导航栏收缩时头部显示的logo
  key: 'avue',//配置主键,目前用于存储
  indexTitle: 'vue-admin-cli',
  lockPage: '/lock', // 锁屏页路径
  tokenTime: 6000,//token过期时间
  Authorization: 'Authorization', // Token请求头名称
  //http的status默认放行不才用统一处理的,
  statusWhiteList: [400],
  //配置首页不可关闭
  isFirstPage: false,
  setting: {
    sidebar: 'vertical',
    tag: true, // 导航标签
    debug: true, // 日志调试
    collapse: true, // 菜单折叠
    search: true, // 菜单搜索
    lock: true, // 屏幕锁定
    screenshot: false, // 意见反馈
    fullscren: true, // 屏幕全屏
    theme: false, // 主题选择
    menu: false, // 顶部菜单
    color: false // 主题颜色
  },
  // 版权信息配置
  copyright: {
    startAge: 2021,
    websiteName: '八斗五车',
    websiteUrl: 'https://www.unboundary.cn'
  },
  // 首页配置
  fistPage: {
    label: '首页',
    value: '/dashboard/index',
    params: {},
    query: {},
    meta: {
      i18n: 'dashboard'
    },
    close: false
  },
  //配置菜单的属性
  menu: {
    iconDefault: 'icon-caidan',
    label: 'label',
    path: 'path',
    icon: 'icon',
    children: 'children',
    query: 'query',
    href: 'href',
    meta: 'meta'
  }
};