/* eslint-disable linebreak-style */
// 配置编译环境和线上环境之间的切换

let baseUrl = '/api'; // api请求接口地址
const env = process.env; // 环境变量
if (env.NODE_ENV == 'development') { // 开发环境
  baseUrl = '/api'; // 开发环境地址
} else if (env.NODE_ENV == 'production') { // 生产环境
  baseUrl = ''; //生产环境地址
} else if (env.NODE_ENV == 'test') { // 测试环境
  baseUrl = ''; //测试环境地址
}
export {
  baseUrl,
  env
};
